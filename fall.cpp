#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <array>
#include <list>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <math.h>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

typedef array<int,4> delta;
constexpr int MAX_CAPACITY  = 10;





istream &operator>>(istream &is, delta &r){
    generate(r.begin(),r.end(), [&is,i=0]() mutable { is >> i; return i; });
    return is;
}

ostream &operator<<(ostream &os, const delta &r){
    os << "[ ";
    auto e = r.end();
    copy(r.begin(), --e, ostream_iterator<int>(os,", ") );
    return os << r.back() << " ]";
}


typedef vector<array<double,4>> cast_matrix;
typedef vector<double> cast_vector;

const cast_matrix I = {
    {-0.5, -0.5, -0.5, -0.5},
    {0, -1, -1, -1},
    {0, 0, -1, -1},
    {0, 0, 0, -1}
};
const cast_matrix Ineg = {
    {-0.5, 0, 0, 0},
    {0, -1, 0, 0},
    {0, 0, -1, 0},
    {0, 0, 0, -1}
};

namespace std {

cast_vector operator*( const cast_matrix & m, const delta &v ){
    cast_vector s(m.size());
    generate( s.begin(), s.end(), 
    [&v,&m,i=0]() mutable { return accumulate( v.begin(), v.end(), 0.0, 
    [&m,j=0,k=i++]( const double o, const int p ) mutable {
            return o + p*m.at(k).at(j++);
    }); });   
    return s;
}

cast_vector operator+(const cast_vector &v1, const cast_vector &v2){
    cast_vector s( v1.size() );
    transform(v1.begin(), v1.end(), v2.begin(), s.begin(), plus<double>());
    return s;
}

delta operator-( const delta& d ) {
    delta s;
    transform(d.begin(), d.end(), s.begin(), [](const int v){return -v;});
    return s;
}
delta operator+(const delta& d1, const delta& d2) {
    delta s;
    transform(d1.begin(), d1.end(), d2.begin(), s.begin(), plus<int>() );
    return s;
}
void operator+=(delta& d1, const delta& d2)
{
  d1 = d1 + d2;
}
delta operator-(const delta& d1, const delta& d2) {
    return d1 + (-d2);
}
bool operator<(const delta& d1, const delta& d2) {
    delta d = d2 + d1;
    return all_of(d.begin(), d.end(), 
    [](const int v) { return v>=0; });
}

bool operator>(const delta& d1, const delta& d2) {
    return !( d1 < d2 );
}
} // !namespace std

ostream &operator<<( ostream &os, const cast_vector &cv ){
    os << "[ ";
    copy(cv.begin(), --cv.end(), ostream_iterator<double>(os, ", "));
    return os << cv.back() << " ]";
}

struct action {
    int id, price, l=0;
    cast_vector dist;
    double value=0.0; 
    delta d;
    string type;

    int tomeIndex; 
    int taxCount; 
    bool castable; 
    bool repeatable;


    action(): type("REST"), id(0), price(0), dist(0) {}
    string str() const { 
        return type + (( type=="REST" ) ? "" : " " + to_string(id)); 
    }
};

istream &operator>>(istream &is, action &a){
    is >> a.id >> a.type >> a.d >> a.price;
    is >> a.tomeIndex >> a.taxCount >> a.castable >> a.repeatable; is.ignore();
    return is;
}

ostream &operator<<(ostream &os, const action &a){
    os << a.type << " " << a.id;
    if (a.type != "NULL") os << " delta: " << a.d << " ";
    if (a.dist.size()) os << "\n\t\tdist: " << a.dist << "\n\t\t"; 
    if (a.price) os << "price: " << a.price;
    if (a.l) os << " length: " << a.l;
    if (a.value) os << " value: " << a.value;
    return os << endl;
}



typedef vector<action> spellBook;

constexpr auto getSpellBooks = [](){
        int actionCount; // the number of spells and recipes in play
        cin >> actionCount; cin.ignore();
        spellBook brew, cast, opp_cast, learn;
        for(;actionCount--;){
            action a;
            cin >> a;
            if ( a.type == "BREW" ) brew.push_back(a);
            else if ( a.type == "CAST" ) cast.push_back(a);
            else if ( a.type == "OPPONENT_CAST" ) opp_cast.push_back(a);
            else if ( a.type == "LEARN" ) learn.push_back(a);
        };

        return make_tuple(brew, cast, opp_cast, learn);
};

ostream &operator<<(ostream &os, const spellBook& sb) {
    os << "\t";
    copy( sb.begin(), --sb.end(), ostream_iterator<action>(os, "\t"));
    return os << sb.back() << endl;
}


const int length( const spellBook &sb, const cast_vector &dist) {
    cast_vector pos(dist.size(),0.0);
    copy_if(dist.begin(), dist.end(), pos.begin(), 
    []( const double d ){ return d>0; });
    double nb_rest = 0;
    for ( int i = 0; i < pos.size(); ++i ) {
        nb_rest = max( nb_rest, pos[i] - sb[i].castable );
    }
    return ((int) ceil(  nb_rest +  accumulate( pos.begin(), pos.end(), 0.0 ))); 
}


const cast_vector distance( const delta &d ) {
    cast_vector s = I * d, _s(s);
    bool _d(false);
    for( int i = s.size(); i--;)  
    { 
        // correct unusable surplus  
        if ( s[i] < 0 ) {
            _d = true;
            delta sn = {0,0,0,0};
            sn[i] = - s[i] / I[i][i];
            s = s + (I * sn); 
        }

    };
    if (_d) {
        cerr << "_d_num +- " << _s;
        cerr << "_d_num ++ " << s << endl;
    }
    return  s;
}



constexpr auto comparableBrew( const delta &init, const spellBook &sb, const bool last_brew) { 
    return [&] ( action& a1, action& a2) {
        a1.dist = distance( a1.d + init );  
        a2.dist = distance( a2.d + init );  
        a1.l = length(sb, a1.dist), a2.l = length(sb, a2.dist); 
        a1.value = ((double) a1.price) / a1.l; 
        a2.value = ((double) a2.price) / a2.l; 
        if ( last_brew )
            return  a1.l < a2.l || ( a1.l == a2.l && a1.price > a2.price );
        else 
            return  a1.l * a2.price < a2.l * a1.price;
        };
}


const auto comparatorLearn = []( action &a1, action &a2 ){    
    a1.dist = I * a1.d; a1.l = - (int) accumulate(a1.dist.begin(), a1.dist.end(), 0.0);
    a2.dist = I * a2.d; a2.l = - (int) accumulate(a2.dist.begin(), a2.dist.end(), 0.0);
    return a1.l > a2.l;
};




const action getAction( const spellBook &sb, const delta &init, action &brew ) {
    if ( brew.d < init )
        return brew; 
    if ( !brew.dist.size() )
        brew.dist = distance( brew.d + init );

    cerr << "_d init: " << init << " brew: " << brew; 

    for (int i=0; i<brew.dist.size(); ++i) {

        cerr << "_d i: " << i << " cast: " << sb[i];
        cerr << "_d castable: " << boolalpha << sb[i].castable << endl;
        cerr << "_d cast.d<init: " << boolalpha << ( sb[i].d < init ) << endl;
        cerr << "_d brew.dist[i]: " << brew.dist[i] << endl << endl;
 
        if ( brew.dist[i] > 0 && sb[i].castable && sb[i].d < init )
            return sb[i];
    }
    
    return action();
}




namespace MC {



  static vector<delta> splls; 

  const auto splls_key_set = []() {
    set<int> ret;
    for ( int i =0; i< splls.size(); ++i ) ret.insert(i);
    return ret;
  };


  struct state 
  {
    state( const delta & d,  const set<int> & s, const list<state> & l = {} ):
      invtry(d), avail(s), ancstr(l) {}

    state( const state & s ):
      invtry(s.invtry), avail(s.avail), ancstr(s.ancstr) 
    { ancstr.push_back(s); }


  public:
    delta         invtry;
    set<int>      avail; 
    list<state>         ancstr;
  };


  struct state_forward : public state
  {

    state_forward( const delta & d, const set<int> & s = splls_key_set() ) : 
      state( d, s ) 
    {}
    state_forward( const state & f, const int i ) :
        state( f )
    { 
      invtry += splls[i]; 
      avail.erase(i);
    }
    state_forward( state && f ) :
      state_forward( f.invtry )
    { swap( ancstr, f.ancstr ); }

    // Invariant, for each n+1 states:
    //  - inventory fit in capacity 
    //  - if origin state is available in n step, new are in n+1 
    //  - inventory is positiv
    list<state_forward> next() &&
    {
      list<state_forward> ret;
      list<int> cpy;

      transform( cpy.begin(),  
          copy_if( avail.begin(), avail.end(), back_inserter(cpy), 
            [&] ( const int i ) { 
              if ( splls[i] < invtry )
              {
                delta s = splls[i] + invtry;
                return accumulate( s.begin(), s.end(), 0 ) < MAX_CAPACITY;
              }
              return false;
              }),
          back_inserter(ret), 
          [&]( const int i ){ return state_forward( *this, i ); });

      ret.push_back( move(*this) );  
      return ret;
    }

  };



  struct state_backward : public state
  {

    state_backward( const delta & d ) : 
      state( d, {} )
    {}
    state_backward( const state & b, const int i ) :
      state( b )
    {
      invtry -= splls[i];
      avail.insert(i);
    }
    state_backward( state && f ) :
      state_backward( b.inventry )
    { swap( ancstr, b.ancstr ); }


    // Invariant
    //  - at least one of the inventory value decreases at step n+1 
    //  - negativ values is inventory required
    //  - inventory required stays within capacity
    //  - the avail set grow by one spell or is cleared
    list<state_backward> next() &&
    {
      list<state_backward> ret;
      list<int> cpy;

      transform( cpy.begin(),  
          copy_if( splls.key_set().begin(), splls.key_set().end(), back_inserter(cpy), 
            [&] ( const int i ) { 
              if ( avail.find(i) == avail.end() && 
                  any_of( invtry.begin(), invtry.end(), 
                  [&i,j=0] ( const double d ) mutable { return d > 0 && splls[i][j++] > 0; }) ) 
              {
                delta s = invtry - splls[i];
                return accumulate( s.begin(), s.end(), 0, 
                    [](const int s, const int v) { return s + ( v < 0 ? -v : 0 ); } ) < MAX_CAPACITY;
              }
              return false;
              }),
          back_inserter(ret), 
          [&]( const int i ){ return { *this, i }; });

      ret.push_back( move(*this) );  
      return ret;
    }
  };


  auto const linear_comparator = []( const state & s1, const state & s2 ) 
  {
    cast_vector d_s1 = I * s1.invtory; 
    cast_vector d_s2 = I * s2.invtory;
    double l_s1 = - (int) accumulate(d_s1.begin(), d_s1.end(), 0.0);
    double l_s2 = - (int) accumulate(d_s2.begin(), d_s2.end(), 0.0);
    return l_s1 > l_s2;
  };



  template< typename state_iterator, class Compare = linear_comparator, 
    size_t max_intermediate_size = 1000, size_t max_final_size = 100 >
  set< state_iterator, Compare > runStep( const state & s0, int steps_number )
  {

     set< state_iterator, Compare > cur, ret;
     ret.insert( { s0.invtry, s0.avail } );

     while ( --step_number > 0 )
     {
        swap( cur, ret );
        ret.clear();
        for_each( cur.begin(), cur.end(), []( state_iterator & s_f0 ) )
        {
          list<state_iterator> sf_n = sf_0.next();
          for_each( sf_n.begin(), sf_n.end(), [&ret]( const state_iterator & s ){ ret.insert(s); });
          if ( ret.size() > max_intermediate_size )
           ret.erase( ret.rbegin(), next(ret.rbegin(), ret.size() - max_intermediate_size ));
        }
     }

     if ( ret.size() > max_final_size )
       ret.erase( ret.rbegin(), next(ret.rbegin(), ret.size() - max_final_size ));
    
     return ret;
  }


   
} // end MC




constexpr int MAX_N_BREW = 6;
constexpr int MAX_LEARN = 3;

int main()
{

    delta   my_inventory, other_inventory;
    int     my_score, other_score;
    int     my_n_brew = 0, other_n_brew = 0;
    int     my_learn = 0;

    // game loop
    while (1) {
        spellBook brew, cast, opp_cast, learn;
        tie(brew, cast, opp_cast, learn) = getSpellBooks();
        cin >> my_inventory >> my_score; cin.ignore();
        cin >> other_inventory >> other_score; cin.ignore();

        cerr << "inventory: "  << my_inventory << endl;
        cerr << "brews: \n" << brew;
        cerr << "casts: \n" << cast;
        cerr << "learn: \n" << learn;

        
        sort( brew.begin(), brew.end(), comparableBrew(my_inventory, cast, (my_n_brew + 1) == MAX_N_BREW ) );

        cerr << "sorted brew: \n" << brew;

        sort( learn.begin(), learn.begin() + 3, comparatorLearn);

        cerr << "sorted learn: \n" << learn;
        
        if ( my_learn < MAX_LEARN ) {
            cout << learn.front().str() << endl;
        }



        action a = getAction( cast, my_inventory, brew.front() );
        if ( a.type == "BREW" ) ++my_n_brew; 
        cout <<  a.str() << endl;
    }
}

